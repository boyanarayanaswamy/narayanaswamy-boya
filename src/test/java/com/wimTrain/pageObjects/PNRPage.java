package com.wimTrain.pageObjects;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.wimTrain.testCases.BaseClass;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class PNRPage extends BaseClass {

	private AndroidDriver driver;

	public PNRPage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@FindBy(xpath = "//android.widget.LinearLayout[@content-desc=\"PNR\"]/android.widget.FrameLayout/android.widget.TextView")
	public MobileElement pnrbtn;

	@FindBy(id = "com.whereismytrain.android:id/pnr_catv")
	public MobileElement pnrtxtfield;

	@FindBy(xpath = "//android.widget.ImageView[@content-desc=\"More options\"]")
	public MobileElement MoreOptions;

	@FindBy(xpath = "hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.TextView")
	public MobileElement ClearPnr;

	public void test(String pnrvalue) {
		pnrbtn.isDisplayed();
		pnrbtn.click();
		pnrtxtfield.sendKeys(pnrvalue);

	}

	public void clearpnrNbr() {
		MoreOptions.click();
		ClearPnr.click();

	}

}
