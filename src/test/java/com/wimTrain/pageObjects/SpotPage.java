package com.wimTrain.pageObjects;

import java.util.List;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.wimTrain.testCases.BaseClass;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SpotPage extends BaseClass {

	public SpotPage(AndroidDriver<MobileElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);

	}

	// Mobile Elements
	@FindBy(id = "com.whereismytrain.android:id/spot_autocompleteFrom")
	public MobileElement fromTxt;

	@FindBy(id = "com.whereismytrain.android:id/spot_autocomplete_to")
	public MobileElement toTxt;

	@FindBy(id = "com.whereismytrain.android:id/swapIcon")
	public MobileElement reverse;

	@FindBy(xpath = "//android.widget.ImageButton[@content-desc=\"Opening navigation drawer\"]")
	public MobileElement hamberger;

	@FindBy(id = "com.whereismytrain.android:id/spot_autocomplete_to")
	public MobileElement languagebtn;

	@FindBy(className = "android.widget.RadioButton")
	public List<MobileElement> langElements;
	
	@FindBy(id = "com.whereismytrain.android:id/spot_findTrains")
	public MobileElement trainsearcBhtn;
	
	@FindBy(xpath = "//android.widget.ImageView[@content-desc=\"More options\"]")
	public MobileElement moreOptions;
	
	@FindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.TextView")
	public MobileElement clearsearcBtn;

	// Methods of spot page

	public void test_reverseFunctionality(String fromValue, String toValue) {

		fromTxt.clear();
		fromTxt.sendKeys(fromValue);
		toTxt.clear();
		toTxt.sendKeys(toValue);
		reverse.click();
	}

	public void test_selectlanguage() {
		hamberger.isDisplayed();
		hamberger.click();
		languagebtn.isDisplayed();
		languagebtn.click();

	}
	
	public void clearrecentsearch() {
		moreOptions.click();
		clearsearcBtn.click();
	}
	

}
