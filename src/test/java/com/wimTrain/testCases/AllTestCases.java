package com.wimTrain.testCases;

import org.testng.annotations.Test;

import com.wimTrain.pageObjects.PNRPage;
import com.wimTrain.pageObjects.SpotPage;

public class AllTestCases extends BaseClass {
	SpotPage spotPage = new SpotPage(driver);
	PNRPage pnrpage = new PNRPage(driver);

	@Test(priority = 0)
	public void testcaseOne() {
		spotPage.test_reverseFunctionality("Hyderabad", "Bangalore");

	}

	@Test(priority = 1)
	public void clearrecentsearches() {
		spotPage.clearrecentsearch();

	}

	@Test(priority = 2)
	public void testpnr() {
		pnrpage.test("Qapitol");

	}

	@Test(priority = 3)
	public void clearpnr() {
		pnrpage.clearpnrNbr();
	}

}
