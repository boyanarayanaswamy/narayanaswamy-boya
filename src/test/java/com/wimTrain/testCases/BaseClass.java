package com.wimTrain.testCases;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import com.wimTrain.pageObjects.Driver;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class BaseClass extends Driver {

	@BeforeClass
	public void setup() throws MalformedURLException {

		DesiredCapabilities caps = new DesiredCapabilities();

		caps.setCapability("deviceName", "Realme");
		caps.setCapability("udid", "a706349a");
		caps.setCapability("platformName", "Android");
		caps.setCapability("platformVersion", "11");
		caps.setCapability("skipUnlock", "true");
		caps.setCapability("appPackage", "com.whereismytrain.android");
		caps.setCapability("appActivity", "com.whereismytrain.view.activities.HomeActivity");
		caps.setCapability("noReset", "true");
		caps.setCapability("automationName", "UiAutomator2");

		driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), caps);
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

	}

	@AfterClass
	public void teardown() {

		driver.quit();
	}
}
